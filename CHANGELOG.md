## Version 1.0.6

- 适配API9Stage模型；


## Version 1.0.5

- hvigor工程结构整改；
- index.ets 内部分文件导出方式修改；

## Version 1.0.3

- 支持SVG图像的显示，可显示静态及动态SVG图像；
- 支持快捷生成SVG图像文件；
- 支持操作SVG图像进行颜色、样式、内容的修改；
- 支持将SVG图像的xml文本解析为可操作对象。