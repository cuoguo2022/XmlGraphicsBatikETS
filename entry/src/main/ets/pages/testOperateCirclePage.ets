/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SVGManager } from '@ohos/XmlGraphicsBatikETS';
import { SVGCircle } from '@ohos/XmlGraphicsBatikETS';
import { SVGSpecifiedFormat } from '@ohos/XmlGraphicsBatikETS';
import { consoleInfo } from '@ohos/XmlGraphicsBatikETS';
import { SVGAttrConstants } from '@ohos/XmlGraphicsBatikETS';
import fileio from '@ohos.fileio';

@Entry
@Component
struct Index {
  private svgManager: SVGManager = SVGManager.getInstance();
  private allAttrCircleObj: object = Object.create(null);
  @State
  private svgUri: string= '';

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Image(this.svgUri)
        .width(250)
        .height(250)
        .backgroundColor(Color.Pink)

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('add circle1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var circle: SVGCircle = new SVGCircle();
            circle.setCX(80);
            circle.setCY(80);
            circle.setR(70);
            circle.addAttribute('style', 'fill:rgb(0,0,255);stroke-width:2;stroke:rgb(0,0,0)')
            var circleObj = circle.toObj();

            var svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('circle');
            svgSpecifiedFormat.setAttributes(circleObj);

            var svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.allAttrCircleObj = svgSpecifiedFormat.toObj();
              this.svgManager.addChildNode(svgRoot, this.allAttrCircleObj);
              consoleInfo('Test circle: add circle1 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('add circle2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var circle: SVGCircle = new SVGCircle();
            circle.setCX(170);
            circle.setCY(80);
            circle.setR(50);
            circle.addAttribute('style', 'fill:rgb(255,0,0);stroke-width:5;stroke:rgb(0,0,255)')
            var circleObj = circle.toObj();

            var svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('circle');
            svgSpecifiedFormat.setAttributes(circleObj);

            var svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.svgManager.addChildNode(svgRoot, svgSpecifiedFormat.toObj());
              consoleInfo('Test circle: add circle2 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })
      }
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('update attr for circle1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test circle: update attr for circle1', 'svg tag is null');
              return false;
            }

            var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test circle: update attr for circle1', `svg tag's elements is null`);
              return false;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
              consoleInfo('Test circle: update attr for circle1', `the elements's type of svg tag is not array`);
              return;
            }

            var circleResult = null;
            try {
              svgElements.forEach((item) => {
                if (typeof item === SVGAttrConstants.TYPEOF_OBJECT) {
                  var nameValue: string = this.svgManager.getValueForKey(item, SVGAttrConstants.ATTR_KEY_NAME);
                  if (nameValue === 'circle') {
                    circleResult = item;
                    throw 'has got circle,jump out';
                  }
                }
              })
            } catch (e) {
              if (!circleResult) {
                consoleInfo('Test circle: update attr for circle1', 'circle not exist');
                return;
              }

              if (typeof circleResult === SVGAttrConstants.TYPEOF_OBJECT) {
                var circleAttributes = circleResult[SVGAttrConstants.ATTR_KEY_ATTRIBUTES];
                circleAttributes['cx'] = 70;
                circleAttributes['cy'] = 50;
                circleAttributes['r'] = 20;
                this.svgManager.setAttribute(circleAttributes, 'style', 'fill:rgb(0,255,0);stroke-width:10;stroke:rgb(0,255,255)');
                this.allAttrCircleObj = circleResult;
              }
              consoleInfo('Test circle: update attr for circle1 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('remove circle2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test circle: remove circle2', 'svg tag is null');
              return;
            }

            var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test circle: remove circle2', `svg tag's elements is null`);
              return;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
              consoleInfo('Test circle: remove circle2', `svg tag's elements is null`);
              return;
            }

            if (svgElements.length >= 2) {
              svgElements.splice(1, 1);
            }
            consoleInfo('Test remove circle2 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Button('show svg with circle')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .width(250)
        .height(50)
        .onClick(() => {
          var svgTotalObj = this.svgManager.getSVGTotalObj();
          var success = function () {
            consoleInfo('Test circle: saveFile', 'success');
          }

          let fileName = new Date().getTime() + 'Circle.svg'
          this.svgManager.saveSVG(fileName, svgTotalObj, success);
          setTimeout(() => {
              consoleInfo('Test circle: show svg getFilePath ', globalThis.filesDir);
              this.svgUri = 'file://' + globalThis.filesDir + '/' + fileName;
          }, 100);
        })


      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove cx attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('cx');
          })

        Button('remove cy attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('cy');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove r attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('r');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    // 清空已存在的SVG根
    this.svgManager.createSVGDeclares();
  }

  removeAttribute(firstAttrName: string, secondAttrName?: string) {
    if (!this.allAttrCircleObj) {
      consoleInfo('test remove ' + firstAttrName, 'circle is not added.');
      return;
    }
    var circleJson = JSON.stringify(this.allAttrCircleObj);
    var circleOriginData = JSON.parse(circleJson);

    var attrs = this.svgManager.getValueForKey(circleOriginData, SVGAttrConstants.ATTR_KEY_ATTRIBUTES);
    if (!attrs) {
      consoleInfo('test remove ' + firstAttrName, 'circle1 has no attributes');
      return;
    }
    this.svgManager.removeByKey(attrs, firstAttrName);
    if (secondAttrName) {
      this.svgManager.removeByKey(attrs, secondAttrName);
    }

    // 替换 circle
    var svgRoot = this.svgManager.getSVGRoot();
    var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);

    if (typeof svgElements === SVGAttrConstants.TYPEOF_OBJECT && Array.isArray(svgElements)) {
      svgElements.splice(0, 1, circleOriginData);
    }
    consoleInfo('test remove attr: ' + firstAttrName, JSON.stringify(this.svgManager.getSVGTotalObj()));
  }

  aboutToDisappear() {
    this.removeExistSVGFile();
  }

  removeExistSVGFile() {
    let dir = fileio.opendirSync(globalThis.filesDir);
    let that = this;
    dir.read()
      .then(function (dirent) {
        if (dirent && dirent.isFile()) {
          fileio.unlink(globalThis.filesDir + '/' + dirent.name, function (removeErr) {
            if (removeErr) {
              consoleInfo('test circle remove file', 'failed');
            }
          })
          that.removeExistSVGFile();
        }
      })
      .catch((err) => {
        consoleInfo('test circle remove file failed', err);
      })
  }
}