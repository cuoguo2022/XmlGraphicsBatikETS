/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SVGManager } from '@ohos/XmlGraphicsBatikETS';
import { SVGEllipse } from '@ohos/XmlGraphicsBatikETS'
import { SVGSpecifiedFormat } from '@ohos/XmlGraphicsBatikETS';
import { consoleInfo } from '@ohos/XmlGraphicsBatikETS';
import { SVGAttrConstants } from '@ohos/XmlGraphicsBatikETS';
import fileio from '@ohos.fileio';

@Entry
@Component
struct Index {
  private svgManager: SVGManager = SVGManager.getInstance();
  private allAttrEllipseObj: object = Object.create(null);
  @State
  private svgUri: string= '';

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Image(this.svgUri)
        .width(250)
        .height(250)
        .backgroundColor(Color.Pink)

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('add ellipse1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var ellipse: SVGEllipse = new SVGEllipse();
            ellipse.setCX(100);
            ellipse.setCY(80);
            ellipse.setRX(70);
            ellipse.setRY(50);
            ellipse.addAttribute('style', 'fill:rgb(0,0,255);stroke-width:2;stroke:rgb(0,0,0)')
            var ellipseObj = ellipse.toObj();

            var svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('ellipse');
            svgSpecifiedFormat.setAttributes(ellipseObj);

            var svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.allAttrEllipseObj = svgSpecifiedFormat.toObj();
              this.svgManager.addChildNode(svgRoot, this.allAttrEllipseObj);
              consoleInfo('Test Ellipse: add ellipse1 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('add ellipse2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var ellipse: SVGEllipse = new SVGEllipse();
            ellipse.setCX(210);
            ellipse.setCY(70);
            ellipse.setRX(40);
            ellipse.setRY(20);
            ellipse.addAttribute('style', 'fill:rgb(255,0,0);stroke-width:5;stroke:rgb(0,0,255)')
            var ellipseObj = ellipse.toObj();

            var svgSpecifiedFormat: SVGSpecifiedFormat = new SVGSpecifiedFormat();
            svgSpecifiedFormat.setElementType(SVGAttrConstants.ATTR_VALUE_ELEMENT);
            svgSpecifiedFormat.setElementName('ellipse');
            svgSpecifiedFormat.setAttributes(ellipseObj);

            var svgRoot = this.svgManager.getSVGRoot();
            if (svgRoot) {
              this.svgManager.addChildNode(svgRoot, svgSpecifiedFormat.toObj());
              consoleInfo('Test ellipse: add ellipse2 svgTotalRoot', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })
      }
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('update attr for ellipse1')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test ellipse: update attr for ellipse1', 'svg tag is null');
              return false;
            }

            var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test ellipse: update attr for ellipse1', `svg tag's elements is null`);
              return false;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
              consoleInfo('Test ellipse: update attr for ellipse1', `the elements's type of svg tag is not array`);
              return;
            }

            var ellipseResult = null;
            try {
              svgElements.forEach((item) => {
                if (typeof item === SVGAttrConstants.TYPEOF_OBJECT) {
                  var nameValue: string = this.svgManager.getValueForKey(item, SVGAttrConstants.ATTR_KEY_NAME);
                  if (nameValue === 'ellipse') {
                    ellipseResult = item;
                    throw 'has got ellipse,jump out';
                  }
                }
              })
            } catch (e) {
              if (!ellipseResult) {
                consoleInfo('Test ellipse: update attr for ellipse1', 'ellipse not exist');
                return;
              }

              if (typeof ellipseResult === SVGAttrConstants.TYPEOF_OBJECT) {
                var ellipseAttributes = ellipseResult[SVGAttrConstants.ATTR_KEY_ATTRIBUTES];
                ellipseAttributes['cx'] = 70;
                ellipseAttributes['cy'] = 50;
                ellipseAttributes['rx'] = 50;
                ellipseAttributes['ry'] = 20;
                this.svgManager.setAttribute(ellipseAttributes, 'style', 'fill:rgb(0,255,0);stroke-width:10;stroke:rgb(0,255,255)');
                this.allAttrEllipseObj = ellipseResult;
              }
              consoleInfo('Test ellipse: update attr for ellipse1 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
            }
          })

        Button('remove ellipse2')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            var svgRoot = this.svgManager.getSVGRoot();
            if (!svgRoot) {
              consoleInfo('Test ellipse: remove ellipse2', 'svg tag is null');
              return;
            }

            var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);
            if (!svgElements) {
              consoleInfo('Test ellipse: remove ellipse2', `svg tag's elements is null`);
              return;
            }

            if (typeof svgElements !== SVGAttrConstants.TYPEOF_OBJECT || !Array.isArray(svgElements)) {
              consoleInfo('Test ellipse: remove ellipse2', `svg tag's elements is null`);
              return;
            }

            if (svgElements.length >= 2) {
              svgElements.splice(1, 1);
            }
            consoleInfo('Test remove ellipse2 svgTotalObj', JSON.stringify(this.svgManager.getSVGTotalObj()));
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Button('show svg with ellipse')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .width(250)
        .height(50)
        .onClick(() => {
          var svgTotalObj = this.svgManager.getSVGTotalObj();
          var success = function () {
            consoleInfo('Test ellipse: saveFile', 'success');
          }

          let fileName = new Date().getTime() + 'Ellipse.svg'
          this.svgManager.saveSVG(fileName, svgTotalObj, success);
          setTimeout(() => {
              consoleInfo('Test ellipse: show svg getFilePath ', globalThis.filesDir);
              this.svgUri = 'file://' + globalThis.filesDir + '/' + fileName;
          }, 100);
        })

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove cx attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('cx');
          })

        Button('remove cy attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('cy');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')

      Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Button('remove rx attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('rx');
          })

        Button('remove ry attr')
          .fontSize(20)
          .fontWeight(FontWeight.Bold)
          .width(200)
          .height(50)
          .onClick(() => {
            this.removeAttribute('ry');
          })
      }
      .margin(10)
      .width('100%')
      .height('50')
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    // 清空已存在的SVG根
    this.svgManager.createSVGDeclares();
  }

  removeAttribute(firstAttrName: string, secondAttrName?: string) {
    if (!this.allAttrEllipseObj) {
      consoleInfo('test remove ' + firstAttrName, 'ellipse is not added.');
      return;
    }
    var ellipseJson = JSON.stringify(this.allAttrEllipseObj);
    var ellipseOriginData = JSON.parse(ellipseJson);

    var attrs = this.svgManager.getValueForKey(ellipseOriginData, SVGAttrConstants.ATTR_KEY_ATTRIBUTES);
    if (!attrs) {
      consoleInfo('test remove ' + firstAttrName, 'ellipse1 has no attributes');
      return;
    }
    this.svgManager.removeByKey(attrs, firstAttrName);
    if (secondAttrName) {
      this.svgManager.removeByKey(attrs, secondAttrName);
    }

    // 替换 ellipse
    var svgRoot = this.svgManager.getSVGRoot();
    var svgElements = this.svgManager.getValueForKey(svgRoot, SVGAttrConstants.ATTR_KEY_ELEMENTS);

    if (typeof svgElements === SVGAttrConstants.TYPEOF_OBJECT && Array.isArray(svgElements)) {
      svgElements.splice(0, 1, ellipseOriginData);
    }
    consoleInfo('test remove attr: ' + firstAttrName, JSON.stringify(this.svgManager.getSVGTotalObj()));
  }

  aboutToDisappear() {
    this.removeExistSVGFile();
  }

  removeExistSVGFile() {
    let dir = fileio.opendirSync(globalThis.filesDir);
    let that = this;
    dir.read()
      .then(function (dirent) {
        if (dirent && dirent.isFile()) {
          fileio.unlink(globalThis.filesDir + '/' + dirent.name, function (removeErr) {
            if (removeErr) {
              consoleInfo('test ellipse remove file', 'failed');
            }
          })
          that.removeExistSVGFile();
        }
      })
      .catch((err) => {
        consoleInfo('test ellipse remove file failed', err);
      })
  }
}