/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SVGManager, consoleInfo } from '@ohos/XmlGraphicsBatikETS';

@Entry
@Component
struct testParseSVGFilePage {
  private svgManager: SVGManager = SVGManager.getInstance();
  @State
  private svgJson: string= '';

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Button('test parse svg file')
        .fontSize(20)
        .fontWeight(FontWeight.Bold)
        .width(250)
        .height(50)
        .onClick(() => {
          this.svgManager.parse('svg.svg', (parseXMLResultObj) => {
            this.svgJson = parseXMLResultObj;
          })
        })

      Text(this.svgJson)
        .width('100%')
        .height('100%')
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    var success = function () {
      consoleInfo('Test parse svg file, saveFile', 'success');
    }
    this.svgManager.saveSVG('svg.svg', globalThis.svgOriginXml, success);
  }
}